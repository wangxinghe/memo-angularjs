'use strict';

var app = angular.module('memoApp', ['LocalStorageModule', 'ngRoute'])

app.controller('mycontroller', function (localStorageService) {
        this.memos = [];

        this.currentId = 0;

        this.memoOnClicked = function (memoId) {
            this.currentId = memoId;
            this.currentMemo = this.findObjectById(memoId);
            this.save();
        };

        this.newOnClicked = function () {
            this.currentMemo = {
                id: this.memos.length, title: this.memos.length, content: 'content', date: (new Date()).getDate()
            };
            this.memos.push(this.currentMemo);

            this.save();
        };

        this.saveOnClicked = function () {

        };

        this.deleteOnClicked = function () {
            if (this.currentMemo != null) {
                var index = this.findIndexByObject(this.currentMemo);
                if (index > -1) {
                    this.memos.splice(index, 1);
                    if (index >= this.memos.length) index = this.memos.length - 1;
                    this.currentMemo = this.findObjectByIndex(index);
                    console.dir(this.currentMemo);
                    this.currentId = this.currentMemo.id;

                    this.save();
                }
            }
        };

        this.helpOnClicked = function () {

        };

        this.aboutOnClicked = function () {

        };

        this.save = function () {
            localStorageService.set('memos', this.memos);
            localStorageService.set('currentMemo', this.currentMemo);
            localStorageService.set('currentId', this.currentId);
        };

        this.restore = function () {
            this.memos = localStorageService.get('memos') == null ? [
                {id: 0, title: 'a', content: 'asfdasdfasdf', date: (new Date()).getDate()},
                {id: 1, title: 'b', content: 'vzxcvasdf', date: (new Date()).getDate()},
                {id: 2, title: 'c', content: '1231asdfasdfasdf', date: (new Date()).getDate()}
            ] :  localStorageService.get('memos');
            this.currentMemo = localStorageService.get('currentMemo');
            this.currentId = localStorageService.get('currentId');
        };

        this.restore();

        this.findObjectById = function (id) {
            if (id == null) return null;

            for (var i = 0; i < this.memos.length; i++) {
                if (this.memos[i].id == id) return this.memos[i];
            }
            return null;
        };

        this.findIndexByObject = function (object) {
            if (object == null) return -1;

            for (var i = 0; i < this.memos.length; i++) {
                if (this.memos[i].title === object.title && this.memos[i].content === object.content && this.memos[i].id === object.id) return i;
            }
            return -1;
        };

        this.findObjectByIndex = function (index) {
            for (var i = 0; i < this.memos.length; i++) {
                if (i === index) return this.memos[i];
            }
            return null;
        };

        this.currentMemo = this.findObjectById(this.currentId);
    }
);